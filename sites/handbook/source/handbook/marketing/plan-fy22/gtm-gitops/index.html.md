---
layout: handbook-page-toc
title: "GitOps GTM Motion"
description: "Details regarding GitOps GTM Motion"
twitter_image: '/images/tweets/handbook-marketing.png'
---

## GitOps GTM Motion

### Integrated campaigns
1. GitOps Campaign
   - Campaign brief (placeholder)

### Sales plays

1. [Sales Playbook for Infrastructure Automation](/handbook/marketing/plan-fy22/gtm-gitops/infra-automation-playbook)
   - [Detailed Resource/Usecase Page](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/gitops/)

Additional sales plays will be added as they're available
