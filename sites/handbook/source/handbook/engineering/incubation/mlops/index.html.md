---
layout: handbook-page-toc
title: ModelOps Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## ModelOps Single-Engineer Group

ModelOps is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation).

This group will be focused on enabling data teams to build, test, and deploy their machine learning models. This will be net new functionality within GitLab and will bridge the gap between DataOps teams and ML/AI within production. ModelOps will provide tuning, testing, and deployment of machine learning models including version control and partial rollout and rollback.

**Model**

Algorithm selection based on data shape and analytics doing parameter tuning, feature selection, and data selection. This includes git-focused functionality for hosting machine learning models. Examples include JupyterHub and Anaconda as well as DVC, Dolt, and Delta lake.

**Train**

Provide data teams with the tools to take raw large data sets, clean / morph / wrangle the data, and import the sanitized data into their models to prepare for deployment. Examples of this include Trifacta, TensorFlow Serving, UbiOps, and Sagemaker.

**Test**

Verify everything works as expected and is ready for deployment. Examples of this include PyTest, PyTorch, Keras, and Scikit-learn.

**Deploy**

Enable data teams to deploy their data models including partial rollout, partial rollback, and versioning of training data. Examples of this include Kubeflow and CML.

Our ModelOps focus could also start with a focus on enabling partners to properly integrate into GitLab. Potential partners include Domino Data Lab, Determined AI, and Maiot.

### Vacancy

We’re currently hiring for this role and looking for someone that understands the market, the opportunities and the complexities to help design, and develop our entry into ModelOps.  You’ll need experience in bringing products to markets, experience with Machine Learning tools, and experience with developing and operate large scale services.  You should know the major competitors and partners in this space, and be able to architect capabilities around hosting machine learning models, training models, test and certification and finally enable data teams to deploy with partial rollout and rollback and versioning,  Our tech stack is Ruby, Go and Vue.js, and you’ll need to work across backend, frontend database and infrastructure to bring this opportunity to market.  

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/uO221nchszs" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

You can apply on our [careers page](https://about.gitlab.com/jobs/careers/).
