# frozen_string_literal: true

module ServiceMaturityHelpers
  GREEN_CHECK = '✅'
  RED_X = '❌'
  WHITE_CIRCLE = '⚪'

  def maturity_score(maturity)
    maturity.take_while(&:passed).last&.name || RED_X
  end

  def maturity_evidence(criterion)
    return "#{WHITE_CIRCLE} (not implemented)" if criterion.passed.nil?
    return RED_X unless criterion.passed

    evidence_links = Array(criterion.evidence).map.with_index do |evidence, i|
      url = evidence.try(:url).presence || evidence

      %W[<a href="#{url}">#{i.succ}</a>]
    end

    evidence_links.unshift(GREEN_CHECK).join(' ')
  end

  def maturity_headers(maturity, index)
    maturity.first.last.flat_map do |level|
      level.criteria.map.with_index do |criterion, i|
        [i.zero? ? [level.name, level.criteria.length] : [nil, 1], [criterion.name, 1]]
      end
    end[index] || [['Oops', 1], ['Broken', 1]]
  end
end
